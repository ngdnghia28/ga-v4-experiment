run
  ```
  npm i
  node batchGet.js
  ```

  requestBody: {
      reportRequests: [
        {
          viewId: '181702392',
          dateRanges: [
            {
              startDate: '2018-03-17',
              endDate: '2018-03-24',
            },
            {
              startDate: '14daysAgo',
              endDate: '7daysAgo',
            },
          ],
          metrics: [
            {
              expression: 'ga:users',
            },
          ],
        },
      ],
    },

  Setting viewId to your google analytics page. (See: https://www.youtube.com/watch?v=x1MljgyLeRM to get your viewId)

  Notice: App not review by google yet so it will ask "This application has not been verified"
  Click "Advanced" then "Go to project-551081414339 (not safe)" to accept authorize